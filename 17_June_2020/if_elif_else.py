users = []
options = '''
    Press 1: To add User
    Press 2: To view users
    Press 3: To remove user
    Press 4: To exit\n
'''
print(options)
while True:
    choice = input("Enter Your Choice: ")

    if choice=="1":
        name = input("Enter username to add: ")
        users.append(name)
        print(name,"added successfully!!!")
        print("* "*10)

    elif choice=="2":
        print("Total Users: {}".format(len(users)))
        for index,value in enumerate(users):
            print("{} : {}".format(index,value))
        print("* "*10)

    elif choice=="3":
        name = input("Enter username to remove: ")
        if name in users:
            users.remove(name)
            print("{} removed successfully".format(name))
        else:
            print("User doesn't exist!!!")

    elif choice=="4":
        break
    else:
        print("Invalid Choice")
        print("* "*10)
        
