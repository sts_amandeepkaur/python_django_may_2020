hobby1 = ["playing","singing","travelling"]
hobby2 =["sleeping","reading"]

##Concatenate
# print(hobby1+hobby2)

## Iteration
# print(hobby2*3)

hobby1[1] = "Gaming" ##Update
hobby1.append("Eating") # Insert at last location
hobby1.insert(1,"Internet") #Insert at given location

print("Before: ",hobby1)
# hobby1.remove("travelling")
# print(hobby1.pop(3),"removed!!!")
# print(hobby1.pop(),"removed!!!")
del hobby1[0]
print(hobby1)

