games = ["PubG", "Soccer","Candy Crush","Temple Run","COD","Clash of Clanes","Cricket"]

##Indexing
# print(games[1])
# print(games[-2])

## Slicing
# print(games[2:])
# print(games[:2])
print(games[:-1])


##Step Size
# print(games[::2])
print(games[::-1])
# print(dir(games))
# print(type(games))

##Membership
print("Soccer" in games)
print("Soccer" not in games)
