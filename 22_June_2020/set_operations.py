s1 =set()
s1.add(10)
s1.add(10)
s1.add(20)
s1.add(10)

# print(s1)

set1 = {10,20,11,10}
set2 = {100,10,20,3}

# u = set1|set2
# u = set1.union(set2)
# print(u)

# i = set1 & set2
# i = set1.intersection(set2)
# print(i)

# d = set1 - set2
# d = set2.difference(set1)
d = set1.symmetric_difference(set2)
print(d)