employee = {
    "emp_name":"Lorem Ipsum",
    "salary":10000,
    "emp_code":"ABC1001",
    "company":"XYZ Tech."
}

# for i in employee.keys():
#     print(i)

# for i in employee:
#     print(i,"=>",employee[i])

# for v in employee.values():
#     print(v)

# for key,value in employee.items():
#     print(key,"=>",value)

keys = ["student_name","roll_no","total_marks","marks_obtained"]
marks={}.fromkeys(keys,"No Value yet")
print(marks)