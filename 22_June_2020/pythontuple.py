ls =[10,49,69]
ls[0]=34

rol_no= (10,20,3,6,8,4,7,8,7)

print(rol_no.count(7))
print(rol_no.count(1000))
print(rol_no.index(3))

#Indexing / slicing/ stepsize
print(rol_no[1])
print(rol_no[-1])
print(rol_no[2:])
print(rol_no[:2])
print(rol_no[::2])
print(rol_no[::-1])

# print(dir(ls))
# print(dir(rol_no))

## rol_no[0]=100 ## 'tuple' object does not support item assignment / immutable
## del rol_no[0]
# print(type(rol_no))