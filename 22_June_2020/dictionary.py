student = {
    "name":"Aman",
    "roll_no":123,
    "is_present":True,
    "subjects":("OOPS","Networking","Computer Graphics"),
    "hobbies":["Playing","Travelling"],
    "technology":{
        "front-end":["HTML","CSS","JS"],
        "back-end":["Python","Django"]
    }
}
# print(student["subjects"][2])
# print(student["technology"]["back-end"][1])
# print(student["name"])
# print(student["roll_no"])
# print(type(student))
# print(dir(student))

##Methods
# print(student.get("name"))
student["name"] = "Peter Parker" #Update
student["fee"] = 10000.5 #Insert
student.update({"address":"Punjab","batch":"2020","name":"Harry Potter"})
student["hobbies"][0]="Reading"
del student["technology"]["front-end"]
student.pop("hobbies")
student.pop("technology")
student.pop("subjects")
student.popitem()
student.popitem()
print(student.keys())
print(student.values())
print(student.items())
st=student.copy()
student.clear()
print(student)
print(st)
