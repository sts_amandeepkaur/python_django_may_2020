def employee(name,age,salary):
    print("Name: {} \nAge: {} \nSalary:Rs.{}/-\n".format(name,age,salary))


## Positional Arguments 
# employee("Harry Potter",22,10000)
# employee(5,"Shinchan Nohara",0)

## Keyword Arguments 
employee(age=22,name="Peter Parker",salary=10000)
employee("Nobita Nobi",age=22,salary=20000)


# Press 1 To create account
# Press 2 To Check balance 
# Press 3 To deposit  
# Press 4 To withdraw 
# Press 5 To exit