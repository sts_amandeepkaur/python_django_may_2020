class Student:
    clz = "XYZ College of Engg."

    def __init__(self,name,roll_no):
        self.nm = name
        print("{} registred successfully with {} roll number in {}".format(name,roll_no,self.clz))
    
    def issue_book(self,bname, isdt,rtdate):
        print("\n{} HAS ISSUED A BOOK, HERE IS DETAILS: {}".format(self.nm,self.clz))
        print("Book Name: {} \nIssue Date:{} \nReturn Date: {}".format(bname,isdt,rtdate))

## Creating Object
s1 = Student("Aman",1001)
s2 = Student("Schinchan",1002)
s3 = Student("Nobita Nobi",1003)

## Change Attributes
s2.clz = "ABCD College of Management"

## Access Member function
s2.issue_book("Computer Programming","24 June 2020", "15 July 2020")
s3.issue_book("Artificial Intelligence","20 June 2020", "11 July 2020")
