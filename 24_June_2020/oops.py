#Creating a class
class Student:
    clz = "ABCD Engg. College"
    
    def information(self):
        print("I am a member function in Student Class")

# Creating Objects
s1 = Student()
s2 = Student()

## Accessing Attributes
print(s1.clz)
s1.information()