class Level1:
    def intro1(self):
        print("FUNCTION OF LEVEL 1")

class Level2(Level1):
    def intro2(self):
        print("FUNCTION OF LEVEL 2")

class Level3(Level2):
    def intro3(self):
        print("FUNCTION OF LEVEL 3")

# obj = Level3()
# obj.intro3()
# obj.intro2()
# obj.intro1()

l2 = Level2()
l2.intro1()
l2.intro2()

