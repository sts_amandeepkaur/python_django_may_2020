class Myclass:
    def __init__(self):
        self.name = "AMAN"
        print("Constructor Function Called!")

    def myfun(self):
        print("It is a member function",self.name)
        return 0

    def __del__(self):
        del self.name
        print("DESTRUCTOR CALLED!!!!")


obj1 = Myclass()
obj2 = Myclass()
obj1.myfun()
obj2.myfun()

