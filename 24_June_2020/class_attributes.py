class Student:
    clz = "XYZ College of Engg."

    def registration(self,name,roll_no):
        self.n = name
        print("{} registred successfully with {} roll number in {}".format(name,roll_no,self.clz))

    def issue_book(self,bname, isdt,rtdate):
        print("\n{} HAS ISSUED A BOOK, HERE IS DETAILS: ".format(self.n))
        print("Book Name: {} \nIssue Date:{} \nReturn Date: {}".format(bname,isdt,rtdate))

## Creating Object
s1 = Student()
s2 = Student()
s3 = Student()

s1.registration("Amandeep",1001)
s2.registration("Peter Parker",1002)
s3.registration("James",1003)

s2.issue_book("Computer Programming","24 June 2020", "15 July 2020")
s3.issue_book("Artificial Intelligence","20 June 2020", "11 July 2020")
