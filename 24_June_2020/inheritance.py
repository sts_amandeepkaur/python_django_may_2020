class Student:
    clz="ABCD COLLEGE"
    def info(self):
        print("MEMBER FUNCTION OF PARENT CLASS")

#Single Inheritance
class Library(Student):
    dep = "CSE"
    def books(self):
        print("MEMBER FUNCTION OF CHILD CLASS")

obj1 = Student()
obj2 = Library()

obj2.books()
obj2.info()
print(obj2.clz)
print(obj2.dep)

print(obj1.clz)
# print(obj1.dep) ## Parent class object an't access properties of child class


# print(isinstance(obj1,Library))