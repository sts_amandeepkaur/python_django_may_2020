class Student:
    def __init__(self):
        self.name = "Amandeep Kaur"

    def abcd(self):
        print("Function of parent class")

class Library(Student):
    def lib(self):
        print(self.name)
        print("Library class")

class Account(Student):
    def fee(self):
        print("Fee details here of",self.name)

obj1 = Library()
obj1.lib()
obj1.abcd()

st = Account()
st.fee()


