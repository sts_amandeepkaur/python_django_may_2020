class Registration:
    clz="ABCD"
    def info(self):
        print("REGISTRATION CLASS")

class Library:
    def book(self):
        print("LIBRARY CODE HERE")

## Multiple Inheritance
class Student(Registration, Library):
    def intro(self):
        print("Hii this is student class")

obj = Student()
obj.intro()
obj.book()
obj.info()

o = Library()
print(dir(o))
print(dir(obj))


