try:
    a = {"name":"Aman","roll":123}
    print(a["name"])
    print(a["email"])

except:
    print("Something wrong in your code!!!")

else:
    print("If there is not an error in try block")

finally:
    print("I will run always!!")
