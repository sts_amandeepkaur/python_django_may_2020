import os

if "test" not in os.listdir():
    os.mkdir("test")
    print("Folder created!!!")
else:
    os.rmdir("test")
    print("Folder deleted!!!")