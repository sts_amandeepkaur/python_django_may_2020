#Open File
file = open("myfile.txt","w")

# Write
# file.write("This code is written by python")
print(file.tell()) ##Pointer location

for i in range(1,11):
    file.write("Number is : {}\n".format(2*i))
    
print(file.tell())
# Close
file.close()

