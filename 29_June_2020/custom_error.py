class MyError(BaseException):
    def __init__(self):
        self.msz = "Error 101: My Exception"

    def __str__(self):
        return self.msz


# raise MyError
try:
    print("WELCOME TO MY CODE")
    nm = input("Enter username: ")
    if nm=="admin":
        print("Welcome Admin")
    else:
        raise MyError 

except MyError as x:
    print(x)
    print("Only Admins are allowed here")