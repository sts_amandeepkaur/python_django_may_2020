import os

# print(os.listdir()) ## To check files in a folder
try:
    file = open("abcd.txt","x")
    file.write("TESTING FILE HANDLING")
    file.close()
    print("File created Successfully!!!")

except FileExistsError:
    print("A file with this name Already exists!")
    os.remove("abcd.txt") ## To remove file
    print("File removed!!!")