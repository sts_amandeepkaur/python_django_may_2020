about = "Python is an general purpose language"
name = "AMANDEEP KAUR"

print(about)
print(len(about))

##Indexing
print(about[0])
print(about[1])

print(about[-1])

##Slicing
print(name[1:])
print(about[:6])
print(about[-3:])
print(about[2:9])

# del about[1] #Python string is an immutable type
about = "0123456789"
## Step Size
print(about[::1])
print(about[::2])
print(about[::3])

print(about[::-1])
print(name[::-2])

### Concatenation

print(about+name)

### Membership
print("z" in about)
print("z" not in about)

if "8" in about:
    print("Item Found")
else:
    print("Item not found")