from django.contrib import admin
from myapp.models import Contact_Us,register,Blog

admin.site.site_header="My Website || Admin"

class Contact_UsAdmin(admin.ModelAdmin):
    fields = ["email","name","contact_number","message"]
    list_display = ["id","name","email","contact_number","added_on"]
    list_filter = ["email","added_on"]
    list_editable = ["name"]
    search_fields = ["name","contact_number"]

admin.site.register(Contact_Us, Contact_UsAdmin)
admin.site.register(register)
admin.site.register(Blog)
