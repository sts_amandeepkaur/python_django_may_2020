from django.db import models
from django.contrib.auth.models import User

class Contact_Us(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField()
    contact_number = models.IntegerField()
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural="Contact Us"

class register(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number = models.IntegerField()
    profile_pic = models.ImageField(upload_to="profile",null=True)

    def __str__(self):
        return self.user.email

class Blog(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE) 
    title = models.CharField(max_length=250)
    text = models.TextField()
    image = models.ImageField(upload_to="blogs/%Y/%m/%d",null=True)
    posted_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.title

    



    