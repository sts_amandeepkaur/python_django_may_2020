from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from myapp.models import Contact_Us, register, Blog
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

def home(request):
    context={}
    if request.method=="POST":
        fname = request.POST["first"]
        lname = request.POST["last"]
        un = request.POST["username"]
        pwd = request.POST["password"]
        em = request.POST["email"]
        con = request.POST["contact"]
        
        #Check if your exist or not
        check = User.objects.filter(username=un)
        if len(check) ==0:
            usr = User.objects.create_user(un,em,pwd)
            usr.first_name = fname
            usr.last_name = lname
            # usr.is_staff=True ## To make staff user
            usr.save()

            reg = register(user=usr, contact_number=con)
            reg.save()
            context["status"] = "{} Thanks for registering with us!!!".format(fname)
            context["color"] = "alert-success" 
        else:
            context["status"] = "User with this username already exists !!!".format(fname)
            context["color"] = "alert-danger" 

    return render(request,"home.html",context)
    
def about(request):
    return render(request,"about.html")

def contactpage(request):
    context = {}
    if request.method=="POST":
        #Get data from HTML
        nm = request.POST["name"]
        em = request.POST["email"]
        cn =request.POST["number"]
        msz = request.POST["message"]

        # Save Data to database (Contact_Us model)
        data=Contact_Us(name=nm, email=em, contact_number=cn, message=msz)
        data.save()
        context["status"] = "Dear {} Thanks for your feedback".format(data.name)

    return render(request,"contact.html", context)


def loginview(request):
    context = {}
    if request.method=="POST":
        un = request.POST["usern"]
        pwd = request.POST["passw"]

        check = authenticate(username=un, password=pwd)
        if check:
            login(request,check)
            return HttpResponseRedirect("/dashboard")
        else:
            context["error"] = "Invalid Login Details"

    return render(request,"login.html",context)

def dashboard(request):
    return render(request,"dashboard.html")

def uslogout(request):
    logout(request)
    return HttpResponseRedirect("/")

def create_blog(request):
    context={}
    if request.method=="POST":
        tit = request.POST["title"]
        des = request.POST["des"]
        
        # login_user = User.objects.get(id=request.user.id)
        ##or
        login_user = get_object_or_404(User, id=request.user.id)
        obj = Blog(user=login_user, title=tit, text=des)
        if "image" in request.FILES:
            img = request.FILES["image"]
            obj.image = img
        obj.save()
        context["status"] = "Blog Added Successfully!"

    return render(request,"create_blog.html",context)

def blogs_view(request):
    context={}
    all = Blog.objects.all().order_by("-id")
    context["all"] = all
    return render(request,"blogs.html",context)

def my_blogs(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")

    context={}
    all = Blog.objects.filter(user__id=request.user.id).order_by("-id")
    context["all"] = all
    return render(request,"myblogs.html",context)

def update(request):
    context={}
    if request.method=="GET":
        bid = request.GET["blog_id"]
        t = request.GET["title"]
        txt = request.GET["desc"]

        # Get Blog Object
        blog_to_update = get_object_or_404(Blog,id=bid)

        # Update Blog data
        blog_to_update.title = t
        blog_to_update.text = txt
        blog_to_update.save()
        context["status"] = "{} Updated Successfully".format(t)
    return HttpResponseRedirect("/my_blogs",context)

def delete_blog(request):
    if "blog_id" in request.GET:
        bid = request.GET["blog_id"]
        blog_to_del = get_object_or_404(Blog,id=bid)
        bname = blog_to_del.title
        blog_to_del.delete()
        return HttpResponse("{} Deleted Successfully".format(bname))
    return HttpResponse("Delete View")