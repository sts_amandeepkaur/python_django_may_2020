
colors = ["red","green","blue"]
mystr = "I am from file 2"

def hello():
    print("Function inside file 2")

print("file2=",__name__)

if __name__=="__main__":
    pin_code = 1092384766378
    print(pin_code)
    print("file 2 running directly")
else:
    print("file2 has been imported in another file")