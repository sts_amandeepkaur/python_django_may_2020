def student(**kwargs):
    # print(kwargs)
    for k,v in kwargs.items():
        print(k,"=>",v)
    print("----------------------------\n")


student(name="AMandeep",profile="Software Developer")
student(first_name="Peter")
student(first_name="Peter",roll_no=121,subjects=["HTML","AI"],fee="Rs. 100000.00/-")