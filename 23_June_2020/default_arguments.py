def area(r=0,pi=3.14):
    print("pi: {}".format(pi))
    print("r: {}".format(r))
    print("Area: {}\n".format(pi*r**2))


# area(3.14,10)
# area(r=10)
# area()

def student(name="Unknown",roll_no=0):
    print("Name: ",name)
    print("Roll No: ",roll_no)

student()
student(roll_no=10)
student("Aman",roll_no=10)