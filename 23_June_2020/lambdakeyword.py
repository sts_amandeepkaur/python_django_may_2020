# def add(a,b):
#     return a+b
# print(add(33,1))

# result=lambda a,b:a+b
# print(result(3,12))

# def is_even(x):
#     if x%2==0:
#         print("Number is even")
#     else:
#         print("Number is odd")

# is_even(130 )

## By using lambda 

# check=lambda x:x%2==0
# check=lambda x:"{} Number is Even".format(x) if x%2==0 else "Number is odd"
check=lambda x:"{} is divisible by 7".format(x) if x%7==0 else "{} is not divisible".format(x)
# print(check(10))
# print(check(5))
# print(check(87654))
# for i in range(50):
#     print(check(i))

a = 10
b = 20