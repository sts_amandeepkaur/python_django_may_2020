marks = [12,21,90,33,49,70,82,13,87]

# filter(function, sequence)

def check(x):
    return x>=33

# data= list(filter(check,marks))
# data = list(filter(lambda x:x>=33,marks))
data = list(filter(lambda x:x%7==0,marks))
print(data)

mystr = "i987y7Au9huhliyMoti7r7966rfAggiuupo8oN"
mydata = list(filter(lambda a:a.isupper(), mystr))
print(mydata)

print("AMAN".isupper())
print("h".isupper())
