from django.shortcuts import render
from django.http import HttpResponse
from myapp.models import Contact_Us

def home(request):
    return render(request,"home.html")
    
def about(request):
    return render(request,"about.html")

def contactpage(request):
    context = {}
    if request.method=="POST":
        #Get data from HTML
        nm = request.POST["name"]
        em = request.POST["email"]
        cn =request.POST["number"]
        msz = request.POST["message"]

        # Save Data to database (Contact_Us model)
        data=Contact_Us(name=nm, email=em, contact_number=cn, message=msz)
        data.save()
        context["status"] = "Dear {} Thanks for your feedback".format(data.name)

    return render(request,"contact.html", context)


