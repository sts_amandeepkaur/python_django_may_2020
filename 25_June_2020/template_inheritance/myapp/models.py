from django.db import models

class Contact_Us(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField()
    contact_number = models.IntegerField()
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural="Contact Us"