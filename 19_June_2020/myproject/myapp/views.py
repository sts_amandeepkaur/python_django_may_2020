from django.shortcuts import render
from django.http import HttpResponse
import datetime

colors = ["red","green","orange","blue","black","#00ffff"]

def index(request):
    curr = datetime.datetime.now().strftime("%d-%m-%y %H:%M:%S")
    #Pass data from python to HTML
    context ={"c":colors,"status":"I am from views.py","date":curr}

    return render(request,"first.html",context) 
    # return HttpResponse("<h1>HEllo World!!!</h1>")

def about(request):
    cont = {"clrs":colors}
    return render(request,"about.html",cont)
