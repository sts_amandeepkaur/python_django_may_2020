from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from myapp.models import Contact_Us, register
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

def home(request):
    context={}
    if request.method=="POST":
        fname = request.POST["first"]
        lname = request.POST["last"]
        un = request.POST["username"]
        pwd = request.POST["password"]
        em = request.POST["email"]
        con = request.POST["contact"]
        
        #Check if your exist or not
        check = User.objects.filter(username=un)
        if len(check) ==0:
            usr = User.objects.create_user(un,em,pwd)
            usr.first_name = fname
            usr.last_name = lname
            # usr.is_staff=True ## To make staff user
            usr.save()

            reg = register(user=usr, contact_number=con)
            reg.save()
            context["status"] = "{} Thanks for registering with us!!!".format(fname)
            context["color"] = "alert-success" 
        else:
            context["status"] = "User with this username already exists !!!".format(fname)
            context["color"] = "alert-danger" 

    return render(request,"home.html",context)
    
def about(request):
    return render(request,"about.html")

def contactpage(request):
    context = {}
    if request.method=="POST":
        #Get data from HTML
        nm = request.POST["name"]
        em = request.POST["email"]
        cn =request.POST["number"]
        msz = request.POST["message"]

        # Save Data to database (Contact_Us model)
        data=Contact_Us(name=nm, email=em, contact_number=cn, message=msz)
        data.save()
        context["status"] = "Dear {} Thanks for your feedback".format(data.name)

    return render(request,"contact.html", context)


def loginview(request):
    context = {}
    if request.method=="POST":
        un = request.POST["usern"]
        pwd = request.POST["passw"]

        check = authenticate(username=un, password=pwd)
        if check:
            login(request,check)
            return HttpResponseRedirect("/dashboard")
        else:
            context["error"] = "Invalid Login Details"

    return render(request,"login.html",context)

def dashboard(request):
    return render(request,"dashboard.html")

def uslogout(request):
    logout(request)
    return HttpResponseRedirect("/")